# Acerca de mí

## Una breve descripción

Soy Desarrollador en Python y otros lenguajes modernos -_y no tan modernos_- con experiencia en consultoría de software para gerenciamiento e implementación de proyectos de Time & Attendance, Control de Accesos, Visitas, Tareas, etc. en [netTime](https://www.grupospec.com/es/productos/software-control-de-presencia) y [SPECManager](https://www.grupospec.com/es/productos/software-control-de-presencia).

Actualmente me encuentro trabajando como desarrollador en [SPEC, SA](https://www.grupospec.com/es), para proyectos en Argentina, Latino América y España.

## Proyectos

A lo largo de mi carrera en [SPEC, SA](https://www.grupospec.com/es) (_2015 \- Actual_) he colaborado en diversos proyectos, tanto de desarrollo como de consultoría. Gran parte de los proyectos de desarrollo son Open Source, pero por cuestiones políticas, está controlado el acceso al código fuente.

En caso de que cuentes con acceso, te brindaré el enlace y una breve descripción de cada uno de ellos.

### Desarrollos
- [Outside works](https://play.google.com/store/apps/details?id=com.spec.mobile.specapp&gl=US): Participación en aplicación de marcaje móvil utilizando [Xamarin Forms](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/).
- [t3-gateway](https://gitlab.com/spec-sa-es/t3-gateway): Gateway para integración de hardware de [SPEC, SA](https://www.grupospec.com/es) y de terceros utilizando Python & [FastAPI](https://fastapi.tiangolo.com/) con soporte de despliegue por contenedores con [Docker](https://www.docker.com/) & [Docker Compose](https://docs.docker.com/compose/).
- [netSync](https://gitlab.com/spec-sa-ar/net_sync): Software para migrar y sincronizar datos entre aplicaciones de [SPEC, SA](https://www.grupospec.com/es) y diversos proveedores, tales como [Visma](https://latam.visma.com/), [Certronic](https://certronic.io/site/), etc, utilizando Python, [Django](https://www.djangoproject.com/) & [Celery](https://docs.celeryq.dev) con soporte de despliegue por contenedores con [Docker](https://www.docker.com/) & [Docker Compose](https://docs.docker.com/compose/).
- [spec-utils](https://gitlab.com/spec-sa-ar/spec-utils): Conjunto de conectores para consumir aplicaciones de [SPEC, SA](https://www.grupospec.com/es) y de terceros desde Python.
  Es utilizado por [SPEC, SA](https://www.grupospec.com/es) en [netSync](https://gitlab.com/spec-sa-ar/net_sync), pero puede ser utilizado por cualquier persona en sus proyectos de integración u otro propósito deseado.
  Es un proyecto Open Source y cualquier persona puede utilizarlo desde [PyPI](https://pypi.org/project/spec-utils/) o acceder a su documentación en [Read the Docs](https://spec-utils.readthedocs.io/en/latest/)
- [netsync-installer](https://gitlab.com/spec-sa-ar/netsync-installer): Deplegador automático de [netSync](https://gitlab.com/spec-sa-ar/net_sync) para Windows, utilizando Python & shell scripts.
- [sgc-web](https://gitlab.com/spec-sa-ar/sgc-web): Sistema de gestión de clases, reportes, presentismo y liquidación para la red de gimnasios [Megatlon](https://www.megatlon.com/) utilizando Python & Django.
- [specmanager-api](https://gitlab.com/spec-sa-ar/specmanager-api): Conjunto de módulos internos programados en LIPS para [SPECManager](https://www.grupospec.com/es/productos/software-control-de-presencia), agregando la capacidad de procesar peticiones HTTP para sincronizar nóminas, fichajes, etc.
- [nt-updater](https://gitlab.com/spec-sa-ar/nt_updater): Actualizador automático para versiones críticas de [netTime](https://www.grupospec.com/es/productos/software-control-de-presencia) en clientes de modelo On-Premise.
- [sigec](https://gitlab.com/spec-sa-ar/sigec): Software interno de gestión de Clientes, Presupuestos, Ofertas, Reparaciones y Facturaciones utilizando Python & Django.


### Consultoría

- [Subterráneos de Buenos Aires, SE](https://www.buenosaires.gob.ar/subte): Control Horario del personal y control de visitantes en diversas plantas.
  Interfaz con software de nómina y Payroll mediante lenguaje interno de [netTime](https://www.grupospec.com/es/productos/software-control-de-presencia).
- [MARS Effem](https://arg.mars.com/es-MX): Control Horario y Accesos del personal en diversas plantas de Argentina.
  Interfaz con software de nómina y Payroll mediante lenguaje interno de [netTime](https://www.grupospec.com/es/productos/software-control-de-presencia).
- [COFCO Group](https://www.cofcointernational.com.ar/): Control de Horario y Accesos del personal en diversas plantas de Argentina y Latam.
   Interfaz con software de nómina y Payroll mediante lenguaje interno de [netTime](https://www.grupospec.com/es/productos/software-control-de-presencia) y [SPECManager](https://www.grupospec.com/es/productos/software-control-de-presencia).
- [Peugeot](https://www.peugeot.com.ar/): Control de Accsos de personal propio y contratas a diversas plantas de Argentina.
  Interfaz con software de nómina, refrigerios y contratas mediante lenguaje interno de [SPECManager](https://www.grupospec.com/es/productos/software-control-de-presencia) (LIPS).
- Otros...

## Educación, cursos y otros

- __Ingeniería electrónica__: Universidad Tecnológica Nacional – FRBA. En curso.
- __Técnico en electrónica__: Instituto Politécnico San Arnoldo Janssen 0418. 2007 – 2013.
- __Educación Polimodal, modalidad producción de bienes y servicios__: Instituto
Politécnico San Arnoldo Janssen 0418. 2007 – 2012.
- __Master Xamarin Forms desde cero con Web API y Procedures(C#)__: Udemy 2022.
- __Curso completo programación .Net/.Net Core/.Net 5 (C#)__: Udemy 2022.
- __Kubernetes, de principiante a experto__: Udemy. 2021.
- __Crea Aplicaciones de escritorio con Python (PyQT)__: Udemy 2020.
- __Profesor de Programación en Python__: Azul School. México. 2019.
- __Gestión de proyectos__: Educación IT. 2017.
- __Curso de modelado y animación 3D__: Universidad Tecnológica Nacional. 2014.
- __Seminario de Windows Server__: Universidad Tecnológica Nacional. 2014.
- __Disertor en VI Festival MiSoL__: Facultad de Ingeniería. Misiones. 2013.
- __Curso Introducción a Base de Datos en Java__: Educación IT. 2011.
- __Curso Introducción al Paradigma de Objetos en Java__: Educación IT. 2011.
- __Curso Java para no programadores__: Educación IT. 2011.


## Conocimientos destacados
- Lenguajes: Python, JS, C# & PHP.
- Frameworks: Django, FastAPI, Xamarin, EF Core, y otros.
- Otros: Docker, Kubernetes, Celery, Kafka, Airflow y más.


## Hobbies

Amante del rock y la música clásica. Toco la guitarra desde muy pequeño y estoy aprendiendo a tocar el cello.
Me gustan los videojuegos y suelo practicar deporte de 3 a 4 veces por semana. Actualmente entreno Padel y espero seguir mejorando :smile:


## Contacto
- Tel.: [+54 (11) 4048-7891](tel:+541140487891).
- Email personal: [lucaslucyk@gmail.com](mailto:lucaslucyk@gmail.com).
- Email corporativo: [llucyk@grupospec.com](mailto:llucyk@grupospec.com).
- Linkedin: [lucaslucyk](https://linkedin.com/lucaslucyk)